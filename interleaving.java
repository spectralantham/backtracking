import java.util.*;
public class interleaving{
    public static Boolean check(String s1,String s2,String s3)
    {
        int c1=0,c2=0;
        int j=0,k=0;
        if(s1==null || s2==null || s3==null)
        {
            return true;
        }
        if(s1.length()!=(s2.length()+s3.length()))
        {
            return false;
        }
        else{
            for(int i=0;i<s1.length();i++)
            {
                if(j<s2.length())
                {
                if(s1.charAt(i)==s2.charAt(j))
                {
                    j++;
                }
                }
                if(k<s3.length())
                {
                if(s1.charAt(i)==s3.charAt(k)){
                    k++;
                }
                }
            }
            if(s2.length()==j && s3.length()==k)
            {
                return true;
            }
            else{
                return false;
            }
        }
    }
    public static void main(String args[])
    {
        String s1,s2,s3;
       Scanner scan=new Scanner(System.in);
       s1=scan.nextLine();
       s2=scan.nextLine();
       s3=scan.nextLine();
       if(check(s1,s2,s3)){
           System.out.println("the given strings are interleaving");
       }
       else{
           System.out.println("given Strings are not inter leaving");
       }
    }
}