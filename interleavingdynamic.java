import java.util.*;
public class interleavingdynamic{
    public static Boolean check(String s1,String s2,String s3)
    {
       if(s1.length()==0 &&s2.length()==0 && s3.length()==0)
       {
           return true;
       }
       if(s1.length()==0)
       {
           return false;
       }
       if(s2.length()!=0 && s1.charAt(0)==s2.charAt(0))
       {
           return check(s1.substring(1),s2.substring(1),s3);
       }
       if(s3.length()!=0 && s1.charAt(0)==s3.charAt(0))
       {
           return check(s1.substring(1),s2,s3.substring(1));
       }
       return false;
    }
    public static void main(String args[])
    {
        String s1,s2,s3;
       Scanner scan=new Scanner(System.in);
       s1=scan.nextLine();
       s2=scan.nextLine();
       s3=scan.nextLine();
       if(check(s1,s2,s3)){
           System.out.println("the given strings are interleaving");
       }
       else{
           System.out.println("given Strings are not inter leaving");
       }
    }
}